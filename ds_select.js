class DS_select
{
	constructor(settings)
	{
		this.args = [];

		this.set_default_settings();

		for (let i in settings)
		{
			this.set(i, settings[i]);
		}


		this.start();
		
	}

	start()
	{
		this.render_html();
		this.render_options();
		this.onkeyup();
		this.onfocus();
		
	}

	set_data(jsonData)
	{
		this.set("total_items_count", jsonData.length);
		this.set("jsonData", jsonData);
		this.render_options();
		this.listen_events();
		this.launch_pagination();
	}


	render_html()
	{
		let target_id 	=  this.get("target_id");

		let html = `
		<div class="ds_select__container">
			<input type="text" value="" placeholder="Placeholder" id="` + target_id + `__input"/>
			<input type="hidden" value="" placeholder="Placeholder" id="` + target_id + `__hidden_input"/>
			<div class="ds_select__dropdown">
				<div class="ds_select__options">			
					No results
				</div>
				<div class="ds_select__pagination" id="` + target_id + `__pagination">

				</div>
			</div>
		</div>
		`;


		document.getElementById( target_id ).innerHTML = html;
	}

	launch_pagination()
	{
		if (this.get("display_pages") == false)
			return false;

		var self = this;

		let target_id = this.get("target_id");

	    this.pagination = new DSPagination({
	        "items_per_page"    : this.get("items_per_page"),
	        "display_pages"     : this.get("display_pages"),
	        "total_items_count" : this.get("total_items_count"),
	        "console_log"       : false,
	        "target_id"         : target_id+"__pagination"
	    });


	    this.pagination.on_page_select = function(page)
	    {
	        self.set("current_page", page);
	        self.render_options();
	    }	    
	}

	onkeyup()
	{
		var self = this;

		var target_id = this.get("target_id");

		let myScript = function() {
			self.filter();
			self.render_options();
		}

		document.getElementById( target_id + "__input" ).addEventListener("keyup", myScript);
	}

	onfocus()
	{
		var target_id = this.get("target_id");

		let myScript = function()
		{
			document.getElementById( target_id ).getElementsByClassName("ds_select__dropdown")[0].classList.add("ds_select__dropdown--show");
		}

		document.getElementById( target_id + "__input" ).addEventListener("focus", myScript);
	}

	hide_dropdown()
	{
		let target_id = this.get("target_id");
		document.getElementById( target_id ).getElementsByClassName("ds_select__dropdown")[0].classList.remove("ds_select__dropdown--show");
	}

	filter()
	{
		var self = this;
		var target_id = this.get("target_id");

		let str = document.getElementById( target_id + "__input" ).value;
		let input_regexp = new RegExp(str.replace(/\s+/, '|'));

		let jsonData = this.get("jsonData");
		let filtered_json_data = [];

		for (let i = 0; i < jsonData.length; i++)
		{
			var row = jsonData[i];
			if (self.filter_row(row, input_regexp) == false)
				continue;

			filtered_json_data.push(row);

		}

		this.set("filtered_json_data", filtered_json_data);
		return filtered_json_data;

	}

	filter_row(row, input_regexp)
	{
		var str = row[this.get("data_field")];

		if (str.search(input_regexp) != -1)
			return true;
		else
			return false;
	}

	render_options()
	{
		let target_id = this.get("target_id");
		let jsonData  = false;

		if (this.get("filtered_json_data") == false)
			jsonData  = this.get("jsonData");
		else
			jsonData  = this.get("filtered_json_data");
		

		if (jsonData == false)
		{
			return false;
		}

		let start = this.get("items_per_page") * this.get("current_page") - this.get("items_per_page");
		let end   = start +  this.get("items_per_page");

		let html = '';

		for (let i = start; i < end; i++)
		{
			let row = jsonData[i];

			if (typeof(row) == "undefined")
				break;

			if (typeof row[this.get("data_field")] == "undefined")
			{
				console.error("There is no arr attr '" + this.get("data_field")+"', continue");
				continue;
			}

			let str = row[this.get("data_field")];

			html += '<div class="ds_select__option" data-index="'+i+'">' + str + '</div>';
		}

		document.getElementById( target_id ).getElementsByClassName("ds_select__options")[0].innerHTML = html;

		this.listen_events();

	}

	listen_events()
	{
		var self = this;

		let target_id = this.get("target_id");
		let options = document.getElementById( target_id ).getElementsByClassName("ds_select__option");

		for (let i = 0; i < options.length; i++)
		{
			let option = options[i];
			
			let option_clicked = function()
			{
				let arr_index = this.getAttribute("data-index");
				let jsonData  = self.get("jsonData");

				self.drawPickedValue(jsonData, jsonData[arr_index]);
				self.on_select(jsonData, jsonData[arr_index]); 
			}

			option.removeEventListener("click", option_clicked);
			option.addEventListener("click", option_clicked);
		}
	}


	drawPickedValue(data, row)
	{
		let target_id = this.get("target_id");

		let val = row[this.get("data_field")];
		let id  = row[this.get("data_field_id")];

		if (typeof id == "undefined")
			id = val;

		document.getElementById( target_id +"__input" ).value = val;
		document.getElementById( target_id +"__hidden_input" ).value = id;

		this.hide_dropdown();
	}


	on_select(data, row)
	{

	}

	set_default_settings()
	{
		this.set("data_field", "value");
		this.set("data_field_id", "id");
		this.set("items_per_page", 		10);
		this.set("current_page", 		1);
		/*
		this.set("console_log", 		false);
		this.set("items_per_page", 		10);
		this.set("current_page", 		1);
		this.set("display_pages", 		10);
		this.set("total_items_count", 	0);		
		this.set("aria-control", "");


		this.set("label_first", "First");
		this.set("label_previous", "Previous");
		this.set("label_last", "Last");
		this.set("label_next", "Next");	
		*/
	}

	set(k, v)
	{
	   this.args[k] = v;
	   this.log("set(), "+k +"= " + v);
	}

	get(k)
	{
	   if (typeof this.args[k] == "undefined")
	   {
	       return false;
	   }

	   return this.args[k]
	}

	log(log, log2)
	{
		if (typeof log2 == "undefined") log2 = '';

		if (this.get("console_log") === true)
			console.log(log, log2);

	}
}