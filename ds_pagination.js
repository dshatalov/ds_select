class DSPagination
{
	constructor(settings)
	{
		this.args = [];

		this.set_default_settings();

		for (let i in settings)
		{
			this.set(i, settings[i]);
		}


		this.start();
		
	}


	set(k, v)
	{
	   this.args[k] = v;
	   	this.log("set(), "+k +"= " + v);
	}

	get(k)
	{
	   if (typeof this.args[k] == "undefined")
	   {
	       return false;
	   }

	   return this.args[k]
	}
	



	start()
	{
	 	if (this.errors_on_start() == true) return false;


	 	this.count_total_pages();
	 	this.generate_pages();
	 	this.render_html();
	 	this.listen_actions();

	 	document.getElementById( this.get("target_id") ).classList.add("ds_pagination");
	 	
	}


	count_total_pages()
	{
		this.set("pages_count",
				Math.ceil(
						this.get("total_items_count") / this.get("items_per_page")
						)
			);	


		this.log("count_total_pages(), pages_count = " + this.get("pages_count") + ", "+this.get("total_items_count") +"/"+ this.get("items_per_page"));
		
	}





	generate_pages()
	{
		var page_offset = Math.ceil(this.get("display_pages")/2);

		this.set("page_offset", page_offset);

		if (this.get("current_page") > page_offset)
		{
			var start 	= this.get("current_page") - page_offset;
			var end 	= this.get("current_page") +  page_offset;
		}
		else
		{
			var start 	= 1;
			var end 	= this.get("display_pages");
		}

		if (end > this.get("pages_count"))
		{
			end = this.get("pages_count");
		}

		var pages = [];

	    this.set("start", start);
	    this.set("end", end);

	    for (var i = start; i <= end; i++)
	    {
	    	pages.push(i);
	    }

	    this.set("pages", pages);	

		let previous_page = this.get("current_page") - 1;
		if (previous_page < 1) previous_page = 1;
		this.set("previous_page", previous_page);

		let next_page = this.get("current_page") + 1;

		if (next_page >= this.get("pages_count")) next_page = this.get("pages_count");
		this.set("next_page", next_page);

		let left_dots_page = start - 1;
		if (left_dots_page < 1) left_dots_page = 1;
		this.set("left_dots_page", left_dots_page);

		let right_dots_page = end + 1;
		if (right_dots_page > this.get("pages_count")) right_dots_page = this.get("pages_count");
		this.set("right_dots_page", right_dots_page);

		this.log(pages);	
	}


	errors_on_start()
	{
		var required_settings = [
			"items_per_page",
			"display_pages",
			"total_items_count",
			"target_id"
		];
		

		let error = false;

		for (let i in required_settings)
		{
			if ( this.get(required_settings[i]) == false )
			{
				console.error("Settings are required. " + required_settings[i]);		
				error = true;
			}
		}

		if (error == true)	
			return true;
		else
			return false;	
	}



	log(log, log2)
	{
		if (typeof log2 == "undefined") log2 = '';

		if (this.get("console_log") === true)
			console.log(log, log2);

	}

	set_default_settings()
	{
		this.set("console_log", 		false);
		this.set("items_per_page", 		10);
		this.set("current_page", 		1);
		this.set("display_pages", 		10);
		this.set("total_items_count", 	0);		
		this.set("aria-control", "");


		this.set("label_first", "First");
		this.set("label_previous", "Previous");
		this.set("label_last", "Last");
		this.set("label_next", "Next");		
	}


	render_html()
	{
		

		let pages 		= this.get("pages");
		let html  		= "";
		let target_id 	=  this.get("target_id");

		if (this.get("aria-control") == false)
			this.set("aria-control",  target_id);

		if (this.get("total_items_count") == 0)
		{
			document.getElementById( target_id).innerHTML = html;
			this.log("No items, insert empty html");
			return true;
		}

		/*
			Left side of paginator
		*/

		let disable_left_side = '';
		let disable_left_dots = '';

		if (this.get("current_page") < 2)
			disable_left_side = 'ds_pagination__item--disable ';

		if (this.get("current_page") < this.get("page_offset")+2)
			disable_left_dots = 'ds_pagination__item--disable ';

	    html += '<a tabindex="0" aria-control="' + this.get("aria-control") + '" class="ds_pagination__item ' + disable_left_side + ' ds_pagination__item--first" data-page=1>' 		+ this.get("label_first") 		+ '</a>';
   		html += '<a tabindex="1" aria-control="' + this.get("aria-control") + '" class="ds_pagination__item ' + disable_left_side + ' ds_pagination__item--previous" data-page='+this.get("previous_page")+'>' 	+ this.get("label_previous") 	+ '</a>';
		html += '<a tabindex="2" aria-control="' + this.get("aria-control") + '" class="ds_pagination__item ' + disable_left_dots + ' ds_pagination__item--left_dots" data-page='+this.get("left_dots_page")+'>...</a>';		


		/* 
			Numaric pages
		*/
		let tabindex = 3;

		html += '<div class="paginator__pages">';

		if (this.get("no_numbers") == 1)
		{
			html += '<div class="paginator__no_numbers_current_page">' + this.get("current_page") + '/' + self.get("pages_count") + '</div>';
		}
		else
		{
			for (let i in pages)
			{
				let active_page = '';

				if (pages[i] == this.get("current_page"))	active_page = 'ds_pagination__item--active';
				
				
				html += '<a class="ds_pagination__item ds_pagination__item--page '+ active_page +'" tabindex="' + tabindex + '" aria-control="' + this.get("aria-control") + '" data-page='+pages[i]+'>' + pages[i] + '</a>';

				tabindex++;
			}	
		}	

		html += '</div>';


		/* 
			Right side of paginator
		*/		
		let disable_right_side = '';
		let disable_right_dots = '';

		if (this.get("current_page") >= this.get("pages_count"))
			disable_right_side = 'ds_pagination__item--disable ';

		if  ( 
				this.get("current_page") >
				( 
					this.get("pages_count") - this.get("page_offset")-1 
				) 
			) 
			{
					
				disable_right_dots = 'ds_pagination__item--disable ';
			}

		html += '<a tabindex="' + (tabindex)   + '" aria-control="' + this.get("aria-control") + '"  class="ds_pagination__item  ' + disable_right_dots + '  ds_pagination__item--right_dots" data-page='+this.get("right_dots_page")+'>...</a>';
	    html += '<a tabindex="' + (tabindex++) + '" aria-control="' + this.get("aria-control") + '"  class="ds_pagination__item  ' + disable_right_side + ' ds_pagination__item--next" data-page='+this.get("next_page")+'>' + this.get("label_next") + '</a>';
   		html += '<a tabindex="' + (tabindex++) + '" aria-control="' + this.get("aria-control") + '"  class="ds_pagination__item  ' + disable_right_side + '  ds_pagination__item--last" data-page='+this.get("pages_count")+'>' + this.get("label_last") + '</a>';	

   		document.getElementById( target_id ).innerHTML = html;
   		
	}

	listen_actions()
	{
		var self = this;
		var pagination_dom_objects = document.getElementById(this.get("target_id")).getElementsByClassName('ds_pagination__item');

		for (let i = 0; i < pagination_dom_objects.length; i++)
		{
			pagination_dom_objects[i].addEventListener("click", function(e)
			{
				let page = this.getAttribute("data-page");

				self.before_page_set(Number(page));
				self.set_page(Number(page));
				self.on_page_select(Number(page));
			});	
		}
	
	
	}		


	// For user outside
	before_page_set(page)
	{

	}

	on_page_select(page)
	{

	}

	set_page(page)
	{
		this.set("current_page", page);
		this.start();
	}

}